﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace bKashMFI.Services
{
    public class CertificateValidationService //: ICertificateValidationService
    {
        public bool ValidateCertificate(X509Certificate2 clientCertificate)
        {
            string[] allowedThumbprints = { "117B7B2BEFE2F67C26F6082A557742BD1B425770", "D2F972BEF53F6D8E686F12EF3AF6A58AAE7C2974" };
            if (allowedThumbprints.Contains(clientCertificate.Thumbprint))
            {
                return true;
            }

            return false;
        }
    }
}
