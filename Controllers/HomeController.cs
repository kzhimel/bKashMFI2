﻿using Microsoft.AspNetCore.Mvc;

namespace bKashMFI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string Get() => "Welcome to PcLinkIT";
    }
}
